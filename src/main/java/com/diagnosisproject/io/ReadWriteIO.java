package com.diagnosisproject.io;

import java.io.IOException;

/**
 * Created by pkuz'tc on 3/29/2016.
 */
public interface ReadWriteIO<T> {
	public void write(T entity, String path);

	public T read(String filePath);

	default public T readWithPattern(String filePath) throws IOException, FileIO.FileInvalidValueException {
		return null;
	}
}
