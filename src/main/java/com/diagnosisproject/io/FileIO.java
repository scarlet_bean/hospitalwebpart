 package com.diagnosisproject.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

import java8.util.Iterators;

/**
 * Created by pkuz'tc on 3/29/2016.
 */
public class FileIO implements ReadWriteIO<Hospital> {

    private final List<Diagnosis> diagnosesList = new ArrayList<>();
    private final StringBuffer writeBuffer = new StringBuffer("");


    private Pattern pattern = null;
    private Matcher matcher = null;





    @Override
    public void write(Hospital entity, String path) {

        writeBuffer.append(entity.getTitle() + "\n");

        java8.util.Iterators.forEachRemaining(entity.getPatients().iterator(),
                (Patient patient) -> {
                    writeBuffer.append(
                            patient.getName() + " " + patient.getSurName()
                                    + " (" + patient.getAddress()
                                    + ") " + patient.getBirthDay().getYear() + "-" + patient.getBirthDay().getMonthOfYear() + "-" + patient.getBirthDay().getDayOfMonth() + "{");
                    Iterators.forEachRemaining(patient.getDiagnosesList().iterator(),
                            (Diagnosis diagnosis) -> {
                                writeBuffer.append(diagnosis.getDate().getYear() + "-" + diagnosis.getDate().getMonthOfYear() + "-" + diagnosis.getDate().getDayOfMonth() + ":"
                                        + diagnosis.getSummary() + ",");
                            });

                    writeBuffer.append("}\n");
                });


        try {
            Files.write(new File(path).toPath(), writeBuffer.toString()
                    .getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Hospital read(String filePath) {
        Hospital hospital = new Hospital();
        hospital.setId(1);
        try {
            List<String> result = Files.readAllLines(new File(filePath).toPath());

            result.stream().forEach(System.out::println);
            Iterator<String> resultIterator = result.iterator();
            if (resultIterator.hasNext()) {
                hospital.setTitle(resultIterator.next());
            }
            
            String item = null;
            String patient = null;
            String diagnoses = null;


            while (resultIterator.hasNext()) {
                item = resultIterator.next();
                patient = item.substring(0, item.indexOf("{"));
                diagnoses = item.substring(patient.length() + 1, item.length() - 1);
                String[] patientData = patient.split(" "); //patient part starts
                Patient newPatient = new Patient();
                newPatient.setId(System.currentTimeMillis());
                newPatient.setName(patientData[0]);
                newPatient.setSurName(patientData[1]);
                newPatient.setAddress(patientData[2]);
                newPatient.setBirthDay(Utils.PARSER.parseDateTime(patientData[3]));

                String[] diagnoesesArray = diagnoses.split(","); // split diagnoses row and then build Diagnosis object

                diagnosesList.clear();
                for (String i : diagnoesesArray) {
                    String[] diagnosesData = i.split(":");
                    Diagnosis newDiagnosis = new Diagnosis();
                    newDiagnosis.setId(System.currentTimeMillis());
                    newDiagnosis.setDate(Utils.PARSER.parseDateTime(diagnosesData[0]));
                    newDiagnosis.setSummary(diagnosesData[1]);
                    diagnosesList.add(newDiagnosis);
                }
                ;
                newPatient.setDiagnosesList(diagnosesList);
                hospital.addPatient(newPatient);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hospital;

    }


    private Pattern fieldPattern = null;
    private Matcher fieldPatternMatcher = null;
    @Override
    public Hospital readWithPattern(String filePath) throws IOException, FileInvalidValueException {
        Hospital newHospital = new Hospital();
        newHospital.setId(1);

        List<String> result = Files.readAllLines(new File(filePath).toPath());

        Iterator<String> resultIterator = result.iterator();
        if (resultIterator.hasNext()) {
            newHospital.setTitle(resultIterator.next());
        }
        
        String patientIData = null;
        while (resultIterator.hasNext()) {
            patientIData = resultIterator.next();
            try {
                pattern = Pattern.compile(Utils.PATIENT_PATTERN);
                matcher = pattern.matcher(patientIData);
                if (!matcher.matches()) {
                    throw new FileInvalidValueException("Patient record ["+patientIData+ "] not matches to pattern "+pattern.pattern());
                }

                for (int i = 1; i < matcher.groupCount(); i++) {
                    fieldPattern = Pattern.compile(getPatternForPatientFields(i));
                    fieldPatternMatcher = fieldPattern.matcher(matcher.group(i));
                    if(!fieldPatternMatcher.matches())
                        throw new FileInvalidValueException("Problem with value " + matcher.group(i));
                }

                Patient newPatient = Patient.create()
                        .setId(System.currentTimeMillis())
                        .setName(matcher.group(1))
                        .setSurName(matcher.group(2))
                        .setAddress(matcher.group(3))
                        .setBirthDay(Utils.PARSER.parseDateTime(matcher.group(4)))
                        .build();
            
                //diagnosis
                String[] diagnoses =  matcher.group(5).split(",");
                Pattern diagnosisPattern = Pattern.compile(Utils.DIAGNOSIS_VALUE_PATTERN);
                Matcher diagnosisMatcher = null;
                for(int i = 0;i< diagnoses.length;i++){
                    try {
                        diagnosisMatcher = diagnosisPattern.matcher(diagnoses[i]);
                        if(!diagnosisMatcher.matches())
                            throw new FileInvalidValueException("[Diagnosis] value ["+diagnoses[i]+"] doen't match pattern "+diagnosisPattern.pattern());

                        String[] diagnosisData = diagnoses[i].split(":");
                        for(int j =0;j< diagnosisData.length;j++){
                            pattern = Pattern.compile(getPatternForDiagnosisFields(i+1));
                            matcher = pattern.matcher(diagnosisData[i]);



                            if(!matcher.matches())
                                throw new FileInvalidValueException("[Diagnosis] value ["+diagnosisData[i]+"] doesn't match pattern "+matcher.pattern().pattern() );
                        }
                        
                        Diagnosis newDiagnosis = Diagnosis.create()
                                .setId(System.currentTimeMillis())
                                .setDate(Utils.PARSER.parseDateTime(diagnosisData[0]))
                                .setSummary(diagnosisData[1])
                                .build();
                        Utils.PATIENT_SERVICE.addDiagnosis(newPatient,newDiagnosis);

                    } catch (FileInvalidValueException ex){
                        ex.printStackTrace();
                        continue;
                    }

                }
                newHospital.addPatient(newPatient);



            } catch (FileInvalidValueException e) {
                e.printStackTrace();
                continue;
            }

        }

        return newHospital;
    }

    public void validateDiagnosis(Pattern diagnosisPattern){}
    
    public String getPatternForPatientFields(int index){
        switch (index){
            case 1: return Utils.NAME_SURNAME_PATTERN;
            case 2: return Utils.NAME_SURNAME_PATTERN;
            case 3: return Utils.ADDRESS_PATTERN;
            case 4: return Utils.DATE_PATTERN;
            default: return null;
        }
    }

    public String getPatternForDiagnosisFields(int index){
        switch (index){
            case 1: return Utils.DATE_PATTERN;
            case 2: return Utils.SUMMARY_PATTERN;
            default: return null;
        }
    }

    public class FileInvalidValueException extends Exception {
        /**
		 * 
		 */
		private static final long serialVersionUID = -5967917442835356538L;

		public FileInvalidValueException(String message) {
            super(message);

        }

    }
}
