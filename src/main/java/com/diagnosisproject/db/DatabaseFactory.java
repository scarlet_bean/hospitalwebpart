package com.diagnosisproject.db;

import java.beans.PropertyVetoException;
import java.sql.SQLException;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.DiagnosisDAOImpl;
import com.diagnosisproject.db.dao.HospitalDAOImpl;
import com.diagnosisproject.db.dao.PatientDAOImpl;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DatabaseFactory {
	private final ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();

	private DatabaseFactory() {
	}

	protected ComboPooledDataSource getComboPooledDataSource() {
		return comboPooledDataSource;
	}

	public static Builder configure() {

		return new DatabaseFactory().new Builder();
	}

	public DAO<Hospital> createHospitalDAO() {

		try {
			return new HospitalDAOImpl(this.comboPooledDataSource.getConnection());
		} catch (SQLException e) {

			e.printStackTrace();
			return null;
		}
	}

	public DAO<Patient> createPatientsDAO() {

		try {
			return new PatientDAOImpl(this.comboPooledDataSource.getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public DAO<Diagnosis> createDiagnosesDAO() {

		try {
			return new DiagnosisDAOImpl(this.comboPooledDataSource.getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public class Builder {

		public Builder setPassword(String password) {
			DatabaseFactory.this.getComboPooledDataSource().setPassword(password);
			return this;
		}

		public Builder setUsername(String username) {
			DatabaseFactory.this.getComboPooledDataSource().setUser(username);
			return this;
		}

		public Builder setDriverClass(String driverClass) {
			try {
				DatabaseFactory.this.getComboPooledDataSource().setDriverClass(driverClass);
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
			return this;
		}

		public Builder setJdbcUrl(String url) {
			DatabaseFactory.this.getComboPooledDataSource().setJdbcUrl(url);
			return this;
		}

		public Builder setMaxStatements(Integer count) {
			DatabaseFactory.this.getComboPooledDataSource().setMaxStatements(count);
			return this;
		}

		public Builder setMaxPoolSize(Integer count) {
			DatabaseFactory.this.getComboPooledDataSource().setMaxPoolSize(count);
			return this;
		}

		public Builder setMinPoolSize(Integer count) {
			DatabaseFactory.this.getComboPooledDataSource().setMinPoolSize(count);
			return this;
		}

		public DatabaseFactory buildFactory() {
			return DatabaseFactory.this;
		}
	}

}
