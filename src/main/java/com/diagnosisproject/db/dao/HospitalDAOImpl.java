package com.diagnosisproject.db.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.diagnosisproject.db.dao.service.PatientHospitalDAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

public class HospitalDAOImpl implements DAO<Hospital>, PatientHospitalDAO {

	private static final String TABLE_NAME = "HOSPITALS";
	private static final String[] PROJECTION = { "ID", "TITLE" };

	private static final String INSERT_QUERY = String.format("INSERT INTO %s (%s,%s) VALUES (?,?)", TABLE_NAME,
			PROJECTION[0], PROJECTION[1]);
	private static final String UPDATE_QUERY = String.format("UPDATE %s SET %s = ?, %s = ? WHERE %s = ?", TABLE_NAME,
			PROJECTION[0], PROJECTION[1], PROJECTION[0]);
	private static final String DELETE_QUERY = String.format("DELETE FROM %s WHERE %s = ?", TABLE_NAME, PROJECTION[0]);
	private static final String SELECT_ALL_QUERY = String.format("SELECT * FROM %s", TABLE_NAME);
	private static final String SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s = ?", TABLE_NAME,
			PROJECTION[0]);
	private static final String SELECT_BY_HOSPITAL_QUERY = String
			.format("SELECT * FROM PATIENTS WHERE HOSPITAL_ID = ?");
	private static final String SELECT_BY_DIAGNOSIS_DATE_BY_HOSPITAL_QUERY = "SELECT DISTINCT PATIENTS.ID, PATIENTS.NAME, PATIENTS.SURNAME, PATIENTS.ADDRESS, PATIENTS.DATE "
			+ " FROM PATIENTS " + " INNER JOIN HOSPITALS ON (PATIENTS.HOSPITAL_ID = HOSPITALS.ID)"
			+ " INNER JOIN DIAGNOSES ON (PATIENTS.ID = DIAGNOSES.PATIENT_ID) "
			+ " WHERE (PATIENTS.HOSPITAL_ID = ?) AND (DIAGNOSES.DATE = ?)";

	private final Connection databaseConnection;

	public HospitalDAOImpl(Connection connection) {
		this.databaseConnection = connection;
	}

	@Override
	public boolean add(Hospital entity, Object... optionals) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(INSERT_QUERY);
			statement.setInt(1, entity.getId());
			statement.setString(2, entity.getTitle());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;
	}

	@Override
	public boolean update(Hospital updatedEntity, Object... optionals) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(UPDATE_QUERY);
			statement.setInt(1, updatedEntity.getId());
			statement.setString(2, updatedEntity.getTitle());
			statement.setInt(3, updatedEntity.getId().intValue());

			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;
	}

	@Override
	public boolean delete(Hospital entity) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(DELETE_QUERY);
			statement.setInt(1, entity.getId());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;
	}

	@Override
	public Hospital get(Hospital entity) {

		Hospital selectedHospital = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_QUERY);
			statement.setInt(1, entity.getId().intValue());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				selectedHospital = Hospital.create().setId(results.getInt(1)).setTitle(results.getString(2)).build();
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return selectedHospital;
	}

	@Override
	public List<Hospital> getAll() {
		List<Hospital> hospitals = new ArrayList<>();
		Hospital newHospital = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_ALL_QUERY);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				newHospital = Hospital.create().setId(results.getInt(1)).setTitle(results.getString(2)).build();
				hospitals.add(newHospital);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return hospitals;
	}

	@Override
	public List<Patient> getPatientsByDiagnosisDateAndHospital(Date date, Hospital hospital) {
		List<Patient> patients = new ArrayList<>();
		Patient newPatient = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_BY_DIAGNOSIS_DATE_BY_HOSPITAL_QUERY);
			statement.setInt(1, hospital.getId());
			statement.setDate(2, date);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				newPatient = Patient.create().setId(results.getLong(1)).setName(results.getString(2))
						.setSurName(results.getString(3)).setAddress(results.getString(4))
						.setBirthDay(Utils.PARSER.parseDateTime(results.getDate(5).toString())).build();
				patients.add(newPatient);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return patients;

	}

	@Override
	public List<Patient> getPatientsByHospital(Hospital hospital) {
		List<Patient> patients = new ArrayList<>();
		Patient newPatient = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_BY_HOSPITAL_QUERY);
			statement.setInt(1, hospital.getId());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				newPatient = Patient.create().setId(results.getLong(1)).setName(results.getString(2))
						.setSurName(results.getString(3)).setAddress(results.getString(4))
						.setBirthDay(Utils.PARSER.parseDateTime(results.getDate(5).toString())).build();
				patients.add(newPatient);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return patients;
	}

	@Override
	public void close() {
		try {
			databaseConnection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
