package com.diagnosisproject.db.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.diagnosisproject.db.dao.service.DiagnosisAttachmentsDAO;
import com.diagnosisproject.db.dao.service.DiagnosisPatientDAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

public class DiagnosisDAOImpl implements DAO<Diagnosis>, DiagnosisPatientDAO, DiagnosisAttachmentsDAO {
	private final Connection databaseConnection;

	private static final String TABLE_NAME = "DIAGNOSES";
	private static final String[] PROJECTION = { "ID", "SUMMARY", "DATE", "PATIENT_ID" };
	private static final String[] ATTACHMENT_PROJECTION = { "ID", "TEXT", "DIAGNOSIS_ID", "DATE" };

	// main queries
	private static final String INSERT_QUERY = String.format("INSERT INTO %s (%s,%s,%s,%s) VALUES (?,?,?,?)",
			TABLE_NAME, PROJECTION[0], PROJECTION[1], PROJECTION[2], PROJECTION[3]);
	private static final String UPDATE_QUERY = String.format("UPDATE %s SET %s = ?, %s = ?,%s = ?,%s = ?  WHERE %s = ?",
			TABLE_NAME, PROJECTION[0], PROJECTION[1], PROJECTION[2], PROJECTION[3], PROJECTION[0]);
	private static final String DELETE_QUERY = String.format("DELETE FROM %s WHERE %s = ?", TABLE_NAME, PROJECTION[0]);
	private static final String SELECT_ALL_QUERY = String.format("SELECT * FROM %s ORDER BY %s DESC", TABLE_NAME,
			PROJECTION[2]);
	private static final String SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s = ? ", TABLE_NAME,
			PROJECTION[0]);
	private static final String SELECT_BY_PATIENT_QUERY = String
			.format("SELECT * FROM %s WHERE %s = ? ORDER BY %s DESC", TABLE_NAME, PROJECTION[3], PROJECTION[2]);

	private static final String INSERT_ATTACHMENT_QUERY = String.format("INSERT INTO %s (%s,%s,%s) VALUES (?,?,?)",
			"attachments", ATTACHMENT_PROJECTION[1], ATTACHMENT_PROJECTION[2], ATTACHMENT_PROJECTION[3]);
	private static final String SELECT_ATTACHMENT_BY_DIAGNOSIS_QUERY = String.format("SELECT * FROM %s WHERE %s = ?",
			"attachments", ATTACHMENT_PROJECTION[2]);

	public DiagnosisDAOImpl(Connection databaseConnection) {
		super();
		this.databaseConnection = databaseConnection;
	}

	@Override
	public boolean add(Diagnosis entity, Object... optionals) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(INSERT_QUERY);
			statement.setInt(1, entity.getId().intValue());
			statement.setString(2, entity.getSummary());
			statement.setDate(3, new Date(entity.getDate().toDate().getTime()));
			statement.setInt(4, ((Patient) optionals[0]).getId().intValue());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;
	}

	@Override
	public boolean update(Diagnosis updatedEntity, Object... optionals) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(UPDATE_QUERY);
			statement.setInt(1, updatedEntity.getId().intValue());
			statement.setString(2, updatedEntity.getSummary());
			statement.setDate(3, new Date(updatedEntity.getDate().toDate().getTime()));
			statement.setInt(4, ((Patient) optionals[0]).getId().intValue());
			statement.setInt(5, updatedEntity.getId().intValue());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;

	}

	@Override
	public boolean delete(Diagnosis entity) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(DELETE_QUERY);
			statement.setInt(1, entity.getId().intValue());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;
	}

	@Override
	public Diagnosis get(Diagnosis entity) {
		Diagnosis selectedDiagnosis = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_QUERY);
			statement.setInt(1, entity.getId().intValue());
			ResultSet results = statement.executeQuery();
			while (results.next()) {

				selectedDiagnosis = Diagnosis.create().setId(results.getLong(1)).setSummary(results.getString(2))
						.setDate(Utils.PARSER.parseDateTime(results.getDate(3).toString())).build();
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return selectedDiagnosis;

	}

	@Override
	public List<Diagnosis> getAll() {
		List<Diagnosis> selectedDiagnoses = new ArrayList<>();
		Diagnosis newDiagnosis = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_ALL_QUERY);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				newDiagnosis = Diagnosis.create().setId(results.getLong(1)).setSummary(results.getString(3))
						.setDate(Utils.PARSER.parseDateTime(results.getDate(2).toString())).build();
				selectedDiagnoses.add(newDiagnosis);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return selectedDiagnoses;
	}

	@Override
	public List<Diagnosis> getDiagnosisByPatient(Patient patient) {
		List<Diagnosis> selectedDiagnoses = new ArrayList<>();
		Diagnosis newDiagnosis = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_BY_PATIENT_QUERY);
			statement.setInt(1, patient.getId().intValue());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				newDiagnosis = Diagnosis
						.create()
						.setId(results.getLong(1))
						.setSummary(results.getString(3))
						.setDate(Utils.PARSER.parseDateTime(results.getDate(2).toString()))
						.build();
				selectedDiagnoses.add(newDiagnosis);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return selectedDiagnoses;

	}

	@Override
	public void close() {
		try {
			databaseConnection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean addAttachmentToDiagnosis(String text, Date date, Diagnosis currentDiagnosis) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(INSERT_ATTACHMENT_QUERY);
			statement.setString(1, text);
			statement.setInt(2, currentDiagnosis.getId().intValue());
			statement.setDate(3, date);
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

		return true;

	}

	@Override
	public List<Map<String, DateTime>> getAllAttachmentsOfDiagnosis(Diagnosis currentDiagnosis) {
		List<Map<String, DateTime>> result = new ArrayList<>();

		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_ATTACHMENT_BY_DIAGNOSIS_QUERY);
			statement.setInt(1, currentDiagnosis.getId().intValue());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				result.add(Collections.singletonMap(results.getString(2),
						Utils.PARSER.parseDateTime(results.getDate(4).toString())));

			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return result;
	}

}
