package com.diagnosisproject.db.dao;

import java.util.List;

public interface DAO<T> {
	public boolean add(T entity, Object... optionals);

	public boolean update(T updatedEntity, Object... optionals);

	public boolean delete(T entity);
	
	public T get(T entity);

	public List<T> getAll();
	
	public void close();

}
