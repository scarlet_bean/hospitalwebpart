package com.diagnosisproject.db.dao.service;

import java.sql.Date;
import java.util.List;

import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;

public interface PatientHospitalDAO{
	public List<Patient> getPatientsByDiagnosisDateAndHospital(Date date, Hospital hospital);
	public List<Patient> getPatientsByHospital( Hospital hospital);
	
}
