package com.diagnosisproject.db.dao.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.diagnosisproject.entities.Diagnosis;

public interface DiagnosisAttachmentsDAO {
	public boolean addAttachmentToDiagnosis(String text,Date date ,Diagnosis currentDiagnosis);
	public List<Map<String,DateTime>> getAllAttachmentsOfDiagnosis(Diagnosis currentDiagnosis);
}
