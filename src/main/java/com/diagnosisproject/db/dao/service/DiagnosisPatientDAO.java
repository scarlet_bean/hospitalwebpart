package com.diagnosisproject.db.dao.service;

import java.util.List;

import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Patient;

public interface DiagnosisPatientDAO {
	public List<Diagnosis> getDiagnosisByPatient(Patient patient);
}
