package com.diagnosisproject.db.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

public class PatientDAOImpl implements DAO<Patient> {
	private final Connection databaseConnection;

	private static final String TABLE_NAME = "PATIENTS";

	private static final String[] PROJECTION = { "ID", "NAME", "SURNAME", "ADDRESS", "DATE", "HOSPITAL_ID" };

	// main queries
	private static final String INSERT_QUERY = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?)",
			TABLE_NAME, PROJECTION[0], PROJECTION[1], PROJECTION[2], PROJECTION[3], PROJECTION[4], PROJECTION[5]);
	private static final String UPDATE_QUERY = String.format(
			"UPDATE %s SET %s = ?, %s = ?,%s = ?, %s = ?,%s = ?, %s = ? WHERE %s = ?", TABLE_NAME, PROJECTION[0],
			PROJECTION[1], PROJECTION[2], PROJECTION[3], PROJECTION[4], PROJECTION[5], PROJECTION[0]);
	private static final String DELETE_QUERY = String.format("DELETE FROM %s WHERE %s = ?", TABLE_NAME, PROJECTION[0]);
	private static final String SELECT_ALL_QUERY = String.format("SELECT * FROM %s ORDER BY %s ASC", TABLE_NAME,
			PROJECTION[2]);
	private static final String SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s = ?", TABLE_NAME,
			PROJECTION[0]);

	public PatientDAOImpl(Connection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	
	@Override
	public boolean add(Patient entity, Object... optionals) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(INSERT_QUERY);
			statement.setInt(1, entity.getId().intValue());
			statement.setString(2, entity.getName());
			statement.setString(3, entity.getSurName());
			statement.setString(4, entity.getAddress());
			statement.setDate(5, new Date(entity.getBirthDay().toDate().getTime()));
			statement.setInt(6, ((Hospital) optionals[0]).getId().intValue());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
		
		return true;
	}

	@Override
	public boolean update(Patient updatedEntity, Object... optionals) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(UPDATE_QUERY);
			statement.setInt(1, updatedEntity.getId().intValue());
			statement.setString(2, updatedEntity.getName());
			statement.setString(3, updatedEntity.getSurName());
			statement.setString(4, updatedEntity.getAddress());
			statement.setDate(5, new Date(updatedEntity.getBirthDay().toDate().getTime()));
			statement.setInt(6, ((Hospital) optionals[0]).getId().intValue());
			statement.setInt(7, updatedEntity.getId().intValue());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
		
		return true;
	}

	@Override
	public boolean delete(Patient entity) {
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(DELETE_QUERY);
			statement.setInt(1, entity.getId().intValue());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
		
		return true;

	}

	@Override
	public Patient get(Patient entity) {
		Patient selectedPatient = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_QUERY);
			statement.setInt(1, entity.getId().intValue());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				selectedPatient = Patient.create()
						.setId(results.getLong(1))
						.setName(results.getString(2))
						.setSurName(results.getString(3))
						.setAddress(results.getString(4))
						.setBirthDay(Utils.PARSER.parseDateTime(results.getDate(5).toString()))
						.build();
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
		
		return selectedPatient;
	}

	@Override
	public List<Patient> getAll() {
		List<Patient> selectedPatients = new ArrayList<>();
		Patient newPatient = null;
		PreparedStatement statement;
		try {
			statement = databaseConnection.prepareStatement(SELECT_ALL_QUERY);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				newPatient = Patient.create().setId(results.getLong(1)).setName(results.getString(2))
						.setSurName(results.getString(3)).setAddress(results.getString(4))
						.setBirthDay(Utils.PARSER.parseDateTime(results.getDate(5).toString())).build();
				selectedPatients.add(newPatient);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

		return selectedPatients;
	}

	@Override
	public void close() {
		try {
			databaseConnection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
