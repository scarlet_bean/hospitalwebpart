package com.diagnosisproject.utils;

import com.diagnosisproject.db.DatabaseFactory;
import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.services.PatientService;
import com.diagnosisproject.services.PatientServiceImpl;

import java.util.List;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by deplague on 4/1/16.
 */
public class Utils {
	// whole patterns for parsing
	public static final String ID_PATTERN = "[\\d]+";
	public static final String NAME_SURNAME_PATTERN = "[A-Z][a-z]+";
	public static final String SUMMARY_PATTERN = "[a-zA-Z ]+";
	public static final String ADDRESS_PATTERN = "[\\w\\W\\d]+";
	public static final String DATE_PATTERN = "[\\d]{4}-[\\d]{2}-[\\d]{2}";

	// patient/diagnoses patterns
	public static final String PATIENT_PATTERN = "([\\w]+)\\s([\\w]+)\\s\\(([\\w\\s]+)\\)\\s([\\d]{4}-[\\d]{2}-[\\d]{2})\\{([\\w\\W]+)\\5*\\}";
	public static final String DIAGNOSIS_VALUE_PATTERN = "([\\d-]+):([a-zA-Z]+)";

	// date parser
	public static final String FORMAT = "yyyy-MM-dd";
	public static final DateTimeFormatter PARSER = DateTimeFormat.forPattern(FORMAT);
	// services
	public static final PatientService PATIENT_SERVICE = new PatientServiceImpl();

	// Pooled datasource configurations
	public static final String USERNAME = "deplague";
	public static final String PASSWORD = "deplague";
	public static final String JDBC_URL = "jdbc:sqlite:file:/home/deplague/my.db";
	public static final String DRIVER_CLASS = "org.sqlite.JDBC";
	public static final Integer MAX_POOL_SIZE = 20;
	public static final Integer MIN_POOL_SIZE = 3;
	public static final Integer MAX_STATEMENTS = 120;
	
	public static final DatabaseFactory DATABASE_FACTORY = DatabaseFactory.configure().setUsername("root")
			.setPassword("").setDriverClass("org.sqlite.JDBC").setJdbcUrl("jdbc:sqlite:file:/home/speedfire/my.db")
			.setMaxPoolSize(20).setMaxStatements(120).setMinPoolSize(3).buildFactory();
	
	public static List<Hospital> getHospitals(){
		DAO<Hospital> dao = DATABASE_FACTORY.createHospitalDAO();
		List<Hospital> lst = dao.getAll();
		dao.close();
		return lst;
	}
	
	public static List<Patient> getPatients(){
		DAO<Patient> dao = DATABASE_FACTORY.createPatientsDAO();
		List<Patient> lst = dao.getAll();
		dao.close();
		return lst;
	}

	
}
