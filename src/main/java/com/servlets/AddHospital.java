package com.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.utils.Utils;


@WebServlet("/AddHospital")
public class AddHospital extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public AddHospital() {
        super();

    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("title");
		Utils.DATABASE_FACTORY.createHospitalDAO().add(Hospital
				.create()
				.setId(Long.valueOf(System.currentTimeMillis()).intValue() * -1)
				.setTitle(title)
				.build());
		request.setAttribute("hospitals", Utils.getHospitals());
		response.sendRedirect(request.getContextPath()+"/index");
	}

}
