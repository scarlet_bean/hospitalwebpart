package com.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.service.DiagnosisPatientDAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class DiagnosesCheck
 */
@WebServlet("/DiagnosesCheck")
public class DiagnosesCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DiagnosesCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long id = Long.valueOf(request.getParameter("id"));
		
		DAO<Diagnosis> diagnosisDAO = Utils.DATABASE_FACTORY.createDiagnosesDAO();
		DiagnosisPatientDAO service =(DiagnosisPatientDAO) diagnosisDAO;
		if(service.getDiagnosisByPatient(Patient
				.create()
				.setId(id)
				.build()).isEmpty()){
			response.getWriter().append("empty");
		} else {
			response.getWriter().append("not");
		}
		
	}

}
