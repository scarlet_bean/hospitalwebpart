package com.servlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.service.DiagnosisAttachmentsDAO;
import com.diagnosisproject.db.dao.service.DiagnosisPatientDAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class DiagnosisServlet
 */
@WebServlet("/DiagnosisServlet")
public class DiagnosisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DiagnosisServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long patientId = Long.valueOf(request.getParameter("pid"));
		DAO<Diagnosis> diagnosisDAO = Utils.DATABASE_FACTORY.createDiagnosesDAO();
		DiagnosisPatientDAO service = (DiagnosisPatientDAO) diagnosisDAO;
		List<Diagnosis> diagnoses = service.getDiagnosisByPatient(Patient.create().setId(patientId).build());
		
		final DiagnosisAttachmentsDAO attachmentService = (DiagnosisAttachmentsDAO)diagnosisDAO;
		
		
		request.setAttribute("hid", request.getParameter("hid"));
		request.setAttribute("pid", request.getParameter("pid"));
		
		diagnoses = diagnoses.stream().map(d->{
			List<Map<String,DateTime>> result = attachmentService.getAllAttachmentsOfDiagnosis(d);
			d.setAttachments(result);
			return d;
		}).collect(Collectors.toList());
		diagnosisDAO.close();
		
		request.setAttribute("diagnoses", diagnoses);
		request.getRequestDispatcher("/diagnosis.jsp").forward(request, response);
	}

	

}
