package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

@WebServlet("/EditPatient")
public class EditPatient extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer pId = Integer.valueOf(req.getParameter("p_id"));
		Integer pIdH = Integer.valueOf(req.getParameter("hid"));
		
		String name = req.getParameter("p_name");
		String surname = req.getParameter("p_surname");
		String address = req.getParameter("p_address");
		String date = req.getParameter("p_date");
		
		DAO<Patient> patientsDAO = Utils.DATABASE_FACTORY.createPatientsDAO();
		
		if(patientsDAO.update(Patient.create()
				.setId(Long.valueOf(pId))
				.setName(name)
				.setSurName(surname)
				.setAddress(address)
				.setBirthDay(Utils.PARSER.parseDateTime(date))
				.build(), Hospital.create().setId(pIdH).build())) {
			patientsDAO.close();	
			resp.sendRedirect(req.getContextPath()+"/PatientsServlet?id=" + pIdH);
			
		}
		
		
	}
	
	

}
