package com.servlets;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.service.DiagnosisAttachmentsDAO;
import com.diagnosisproject.db.dao.service.DiagnosisPatientDAO;
import com.diagnosisproject.db.dao.service.PatientHospitalDAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.io.IOContext;
import com.diagnosisproject.io.JsonIO;
import com.diagnosisproject.utils.Utils;

@WebServlet("/serialize")
public class SerializeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SerializeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DAO<Hospital> hospitalDAO = Utils.DATABASE_FACTORY.createHospitalDAO();
		DAO<Diagnosis> diagnosisDAO = Utils.DATABASE_FACTORY.createDiagnosesDAO();

		List<Hospital> hstls = hospitalDAO.getAll().stream().map(i -> {
			List<Patient> ptnts = ((PatientHospitalDAO) hospitalDAO).getPatientsByHospital(i);
			i.setPatients(ptnts.stream().map(patient -> {
				List<Diagnosis> diagnosis = ((DiagnosisPatientDAO) diagnosisDAO).getDiagnosisByPatient(patient);
				patient.setDiagnosesList(diagnosis.stream().map(diag -> {
					diag.setAttachments(((DiagnosisAttachmentsDAO) diagnosisDAO).getAllAttachmentsOfDiagnosis(diag));
					return diag;
				}).collect(Collectors.toList()));
				return patient;
			}).collect(Collectors.toList()));
			return i;
		}).collect(Collectors.toList());
		System.out.println(hstls.get(0).getPatients().get(0).getDiagnosesList().get(0).getAttachments().get(0));
		IOContext<Hospital> context = new IOContext<>(new JsonIO());
		hstls.forEach(hsptl -> {
			try {
				context.write(hsptl, String.format("%s%d.json", "result",
						System.currentTimeMillis() % 1000));
			} catch (Exception e) {
				System.out.println("lol");
				e.printStackTrace();
			}
		});

		response.getWriter().append("SERIALIZED!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
