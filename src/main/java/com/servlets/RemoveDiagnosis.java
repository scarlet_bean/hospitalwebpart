package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class RemoveDiagnosis
 */
@WebServlet("/RemoveDiagnosis")
public class RemoveDiagnosis extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RemoveDiagnosis() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DAO<Diagnosis> diagnosisDAO = Utils.DATABASE_FACTORY.createDiagnosesDAO();
		Long pid = Long.valueOf(request.getParameter("pid"));
		Long hid = Long.valueOf(request.getParameter("hid"));

		if (diagnosisDAO.delete(Diagnosis.create().setId(Long.valueOf(request.getParameter("id"))).build())) {

			response.sendRedirect(String.format(request.getContextPath()+"/DiagnosisServlet?pid=%d&hid=%d", pid, hid));
		}
		;
	}


}
