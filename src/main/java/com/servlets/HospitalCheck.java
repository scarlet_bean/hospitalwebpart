package com.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.service.PatientHospitalDAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class HospitalCheck
 */
@WebServlet("/HospitalCheck")
public class HospitalCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HospitalCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer hospitalID = Integer.valueOf(request.getParameter("id"));
		
		DAO<Hospital> hDAO = Utils.DATABASE_FACTORY.createHospitalDAO();
		PatientHospitalDAO service = (PatientHospitalDAO) hDAO;
		
		if(service.getPatientsByHospital(Hospital.create().setId(hospitalID).build()).isEmpty())
			response.getWriter().append("empty");
		else{
			response.getWriter().append("not");
		}
	}


}
