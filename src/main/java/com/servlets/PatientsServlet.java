package com.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.service.PatientHospitalDAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

@WebServlet("/PatientsServlet")
public class PatientsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public PatientsServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.valueOf(request.getParameter("id"));
		DAO<Hospital> dao = Utils.DATABASE_FACTORY.createHospitalDAO();
		PatientHospitalDAO service = (PatientHospitalDAO) dao;
		
		List<Patient> patients = service.getPatientsByHospital(Hospital
				.create()
				.setId(id)
				.build());
		
		dao.close();
		
		request.setAttribute("patients", patients);
		request.setAttribute("hid",id);
		request.getRequestDispatcher("/patients.jsp").forward(request, response);
		
	}



}
