package com.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class RemoveHospital
 */
@WebServlet("/RemoveHospital")
public class RemoveHospital extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RemoveHospital() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Utils.DATABASE_FACTORY.createHospitalDAO()
				.delete(Hospital.create().setId(Integer.valueOf(request.getParameter("id"))).build());
		request.setAttribute("hospitals", Utils.getHospitals());
		response.sendRedirect(request.getContextPath()+"/index");
	}

	

}
