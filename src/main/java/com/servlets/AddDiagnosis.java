package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

@WebServlet("/AddDiagnosis")
public class AddDiagnosis extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddDiagnosis() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String summary = request.getParameter("summary");
		String date = request.getParameter("date");
		Long pid = Long.valueOf(request.getParameter("pid"));
		Long hid = Long.valueOf(request.getParameter("hid"));

		DAO<Diagnosis> diagnosisDAO = Utils.DATABASE_FACTORY.createDiagnosesDAO();
		if (diagnosisDAO.add(Diagnosis.create().setId(System.currentTimeMillis()).setSummary(summary)
				.setDate(Utils.PARSER.parseDateTime(date)).build(), Patient.create().setId(pid).build())) {

			diagnosisDAO.close();

			response.sendRedirect(
					String.format(request.getContextPath() + "/DiagnosisServlet?pid=%d&hid=%d", pid, hid));
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
