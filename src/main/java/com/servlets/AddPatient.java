package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class AddPatient
 */
@WebServlet("/AddPatient")
public class AddPatient extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPatient() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String patientName = request.getParameter("name");
		String patientSurname = request.getParameter("surname");
		String patientAddress = request.getParameter("address");
		String patientDate = request.getParameter("date");
		Integer hospitalId = Integer.valueOf(request.getParameter("hid"));
		
		Hospital h = Hospital.create().setId(hospitalId).build();
		DAO<Patient> patientsDAO = Utils.DATABASE_FACTORY.createPatientsDAO();
		patientsDAO.add(Patient
				.create()
				.setId(System.currentTimeMillis())
				.setName(patientName)
				.setSurName(patientSurname)
				.setAddress(patientAddress)
				.setBirthDay(Utils.PARSER.parseDateTime(patientDate))
				.build(), h);
		response.sendRedirect(request.getContextPath()+"/PatientsServlet?id="+hospitalId);

	}

}
