package com.servlets;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.db.dao.service.DiagnosisAttachmentsDAO;
import com.diagnosisproject.entities.Diagnosis;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class AddAttachmnet
 */
@WebServlet("/AddAttachment")
public class AddAttachment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddAttachment() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long pid = Long.valueOf(request.getParameter("pid"));
		Long hid = Long.valueOf(request.getParameter("hid"));
		
		Integer id = Integer.valueOf(request.getParameter("d_id"));
		String text = String.valueOf(request.getParameter("text"));
		Date date = new Date(Utils.PARSER.parseDateTime(request.getParameter("attach_date")).getMillis());

		DAO<Diagnosis> diagnosisDAO = Utils.DATABASE_FACTORY.createDiagnosesDAO();
		DiagnosisAttachmentsDAO dao = (DiagnosisAttachmentsDAO) diagnosisDAO;
		if(dao.addAttachmentToDiagnosis(text, date, Diagnosis.create().setId(Long.valueOf(id)).build())){
			diagnosisDAO.close();
			response.sendRedirect(String.format(request.getContextPath()+"/DiagnosisServlet?pid=%d&hid=%d",pid,hid));
		}
		diagnosisDAO.close();
		
	}

}
