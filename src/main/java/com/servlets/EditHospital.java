package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.utils.Utils;

@WebServlet("/EditHospital")
public class EditHospital extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public EditHospital() {
		super();
		// TODO Auto-generated constructor stub
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DAO<Hospital> dao = Utils.DATABASE_FACTORY.createHospitalDAO();
		Integer id = Integer.valueOf(request.getParameter("id"));
		Hospital editableHospital = dao.get(Hospital.create().setId(id).build());
		editableHospital.setTitle(request.getParameter("title"));
		dao.update(editableHospital);
		dao.close();

		request.setAttribute("hospitals", Utils.getHospitals());
		response.sendRedirect(request.getContextPath()+"/index");
	}

}
