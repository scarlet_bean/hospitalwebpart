package com.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Hospital;
import com.diagnosisproject.utils.Utils;

import java8.util.J8Arrays;




@WebServlet("/HospitalUniqueCheck")
public class HospitalUniqueCheck extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = req.getParameter("title");
		DAO<Hospital> dao = Utils.DATABASE_FACTORY.createHospitalDAO();
		List<Hospital> hospitals = dao.getAll();
		dao.close();

		if(J8Arrays.stream(hospitals.toArray()).filter(i->((Hospital) i).getTitle().equals(title)).toArray().length==0){
			resp.getWriter().print("true");
		} else {
			resp.getWriter().print("false");
		}

	}

}
