package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

/**
 * Servlet implementation class RemovePatient
 */
@WebServlet("/RemovePatient")
public class RemovePatient extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RemovePatient() {
		super();		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DAO<Patient> dao = Utils.DATABASE_FACTORY.createPatientsDAO();
		dao.delete(Patient.create().setId(Long.valueOf(Integer.valueOf(request.getParameter("id")))).build());
		dao.close();
		response.sendRedirect(request.getContextPath()+"/PatientsServlet?id=" + request.getParameter("hid"));
	}

	

}
