package com.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static com.diagnosisproject.utils.Utils.PARSER;
import com.diagnosisproject.db.dao.DAO;
import com.diagnosisproject.entities.Patient;
import com.diagnosisproject.utils.Utils;

@WebServlet("/PatientUniqueCheck")
public class PatientUniqueCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String patientName = request.getParameter("name");
		String patientSurname = request.getParameter("surname");
		String patientAddress = request.getParameter("address");
		String patientDate = request.getParameter("date");


		DAO<Patient> dao = Utils.DATABASE_FACTORY.createPatientsDAO();
		List<Patient> patients = dao.getAll();
		if(patients.stream().filter(item->item.getName().equals(patientName) && 
									   item.getSurName().equals(patientSurname) && 
									   item.getAddress().equals(patientAddress) && 
									   item.getBirthDay().equals(PARSER.parseDateTime(patientDate)))
				.toArray().length == 0) 
			response.getWriter().print("true");
		else 
			response.getWriter().print("false");
	}

}
