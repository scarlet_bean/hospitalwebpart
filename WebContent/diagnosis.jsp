<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="com.diagnosisproject.entities.Diagnosis"%>
<%@ page import="org.joda.time.DateTime"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Diagnosis</title>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/materialize.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/materialize.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/gen_validatorv4.js"></script>

<link rel='stylesheet prefetch'
	href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css">
<script src="${path}/Project1/js/diagnosis.js" type="text/javascript"> </script>

<style type="text/css">
body {
	display: flex;
	min-height: 100vh;
	flex-direction: column;
}

main {
	flex: 1 0 auto;
}
</style>

</head>

<body>
	<c:set var="path" value="${pageContext.request.contextPath}" />

	<nav>

	<div class="nav nav-wrapper green darken-5">
		<div class="brand-logo right">Diagnoses</div>
		<div class="col s6 l12 ">
			<a href="${path}/index" class="breadcrumb"> <i
				class="large material-icons">language</i> Hospitals
			</a> <a href="${path}/PatientsServlet?id=${hid}" class="breadcrumb">
				Patients</a> <a href="" data-activates="diagnosis_menu"
				class="dropdown-button breadcrumb">Diagnosis</a>

		</div>

		<ul id="diagnosis_menu" class="dropdown-content">
			<li><a href="#modal_add" class="modal-trigger">Add diagnosis</a></li>
		</ul>
	</div>
	</nav>


	<form name="add_form"
		action="${path}/AddDiagnosis?pid=${pid}&hid=${hid}" method="post">
		<div id="modal_add" class="modal bottom-sheet">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col s12 l12 m12">
							<h4>New Diagnosis</h4>
						</div>
					</div>

					<div class="row">
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">question_answer</i> <input
								id="diagnosis_summary" required maxlength="40" length="40"
								pattern="^[a-z,A-Z 0-9]+$" class="validate" name="summary"
								type="text" /> <label
								data-error="can be only nums,alphs and spaces"
								for="diagnosis_summary">Summary</label>
						</div>
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">today</i> <input
								id="diagnosis_date" name="date" data-type="date" type="text"
								class="validate" required /> <label for="diagnosis_date"
								class="active">Date of diagnosis</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<input class="btn btn-flat modal-action waves-effect waves-red"
					type="submit" value="Save" />
			</div>
		</div>
	</form>

	<form action="${path}/AddAttachment?pid=${pid}&hid=${hid}"
		method="post">
		<div id="modal_add_attach" class="modal bottom-sheet">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col s12 l12 m12 right">
							<h4>New Attachment to Diagnosis</h4>
						</div>
					</div>

					<div class="row">
						<input id="d_id" name="d_id" type="hidden">
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">question_answer</i> <input
								id="attachment_summary" class="validate" pattern="[a-zA-Z ,\d]+"
								name="text" title="Uncorrect message" type="text" length="60"
								required /> <label data-error="Problem with summary"
								for="diagnosis_summary">Text</label>
						</div>
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">today</i> <input
								id="attachment_date" name="attach_date" type="text"
								data-type="date" title="enter date" class="validate" required />
							<label class="active" data-error="Problem date"
								for="diagnosis_date">Date of adding</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<input class="btn btn-flat modal-action waves-effect waves-red"
					type="submit" value="Save" />
			</div>
		</div>
	</form>


	<!-- MAIN CONTENT -->
	<c:choose>
		<c:when test="${!diagnoses.isEmpty()}">
			<main class="">
			<div class="row">
				<div class="col s12 m9 s10">
					<ul class="collapsible popout" data-collapsible="accordion">
						<%
							List<Diagnosis> diagnoses = (List<Diagnosis>) request.getAttribute("diagnoses");
									for (Diagnosis diagnosis : diagnoses) {
										pageContext.setAttribute("id", diagnosis.getId());
										pageContext.setAttribute("summary", diagnosis.getSummary());
						%>

						<li>
							<div class="collapsible-header">
								<%=diagnosis.getDate().toString("yyyy-MM-dd") + " : date of diagnosis"%>
								<a href="#modal_delete" 
									data-did="${id}" 
									onclick="setDeletingId(event)"
									title="Remove diagnosis"
									class="right modal-trigger tooltipped]"
									data-tooltip="Forgot diagnosis" 
									data-position="right">Expire</a>
							</div>
							
							<div class="collapsible-body">
								<a href="#modal_add_attach" data-id="${id}" onclick="getId(event)"
									class="right modal-trigger">Add attachment</a>
								<p class="flow-text">${summary}</p>
								<%
									if (!diagnosis.getAttachments().isEmpty()) {
								%>
								<table
									class="highlight striped bordered responsive-table centered">
									<thead>
										<tr>
											<th>Date of attachment</th>
											<th>Summary</th>
										</tr>
									</thead>
									<tbody>
										<%
											for (Map<String, DateTime> map : diagnosis.getAttachments()) {
										%>

										<tr>
											<td><%=map.values().iterator().next().toString("yyyy-MM-dd")%></td>
											<td><%=map.keySet().iterator().next()%></td>
										</tr>

										<%
											}
										%>
									</tbody>
								</table>
								<%
									}
								%>
							</div>
						</li>
						<%
							}
						%>
					</ul>

				</div>
				<div class="col m3 s2">
					<div class="fixed-action-btn horizontal">

						<div class="fixed-action-btn horizontal"
							style="bottom: 45px; right: 24px;">
							<a class="btn-floating btn-large red"> <i
								class="large material-icons">format_quote</i>
							</a>
							<ul>

								<li><a href="#modal_add" data-tooltip="add new patient"
									data-position="left"
									class="modal-trigger tooltipped btn-floating yellow darken-1"><i
										class="material-icons">mode_edit</i></a></li>
								<li><a href="${path}/PatientsServlet?id=${hid}"
									class="btn-floating blue"><i class="material-icons">trending_down</i></a></li>
								<li><a href="${path}/index" class="btn-floating green"><i
										class="material-icons">publish</i></a></li>
								

							</ul>
						</div>
					</div>
				</div>
			</div>
			</main>
		</c:when>
		<c:when test="${ diagnoses.isEmpty()}">
			<main>
			<p class="flow-text">There is no any diagnosis.</p>
			</main>
		</c:when>

	</c:choose>

	<div id="modal_delete" class="modal">
		<div class="modal-content">
			<h4>Delete diagnosis</h4>
			<p class="flow-text">Do you agree with removing current
				diagnosis?</p>
		</div>

		<div class="modal-footer">
			<a id="delete_reference"
				href="${path}/RemoveDiagnosis?pid=${pid}&hid=${hid}"
				class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
		</div>
	</div>


	<script
		src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>


	<script src="${pageContext.request.contextPath}/js/index.js"></script>

	<footer class="page-footer green">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Footer Content</h5>
				<p class="grey-text text-lighten-4">You can use rows and columns
					here to organize your footer content.</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Links</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="www.google.com">Google</a></li>
					<li><a class="grey-text text-lighten-3" href="www.vk.com">VK</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Сайти
							для душевнохворих</a></li>
					<li><a class="grey-text text-lighten-3" href="www.twitter.com">Twitter</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			© 2014 Copyright Text <a class="grey-text text-lighten-4 right"
				href="#!">More Links</a>
		</div>
	</div>
	</footer>


</body>
</html>