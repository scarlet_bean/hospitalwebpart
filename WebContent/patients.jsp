<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Patients</title>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/materialize.js"></script>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/materialize.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/gen_validatorv4.js"></script>

<link rel='stylesheet prefetch'
	href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css">

<script src="${path}/Project1/js/patients.js" type="text/javascript"> </script>

<style type="text/css">
body {
	display: flex;
	min-height: 100vh;
	flex-direction: column;
}

main {
	flex: 1 0 auto;
}
</style>

</head>

<body>

	<c:set var="path" value="${pageContext.request.contextPath}" />
	<input type="hidden" id="hidden_hid" value="${hid}" />
	<nav>

	<div class="nav nav-wrapper green darken-5">
		<div class="brand-logo right">Patients</div>
		<div>
			<a href="${path}/index" class="breadcrumb"> <i
				class="large material-icons">language</i> Hospitals
			</a> <a href="" data-activates="patient_menu"
				class="dropdown-button breadcrumb">Patients</a>
			

		</div>

		<ul id="patient_menu" class="dropdown-content">
			<li><a href="#modal_add" class="modal-trigger">Add patient</a></li>
		</ul>
		
	</div>
	</nav>


	<form name="add_form" action="${path}/AddPatient?hid=${hid}"
		method="post">
		<div id="modal_add" class="modal bottom-sheet">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col s12 l12 m12">
							<h4>New Patient</h4>
						</div>
					</div>
					<input type="hidden" value="${hid}" name="hos_id" id="hos_id">
					<div class="row">
						<div class="input-field col l6 m6 s6">

							<i class="material-icons prefix">recent_actors</i> <input
								id="patient_name" class="validate" title="Patient name"
								length="40" pattern="[a-zA-Z ]+" name="name" required
								type="text" /> <label
								data-error="Not correct name.Must consist of alphs and space"
								for="patient_name">Name</label>
						</div>

						<div class="input-field col l6 m6 s6">

							<i class="material-icons prefix">subtitles</i> <input
								pattern="[a-zA-Z ]+" id="patient_surname" required length="40"
								class="validate" name="surname" type="text" /> <label
								data-error="Not correct last name.Must consist of alphs and space"
								for="patient_surname">Surname</label>
						</div>
					</div>
					<div class="row">

						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">contact_phone</i> <input
								id="patient_address" required length="40" class="validate"
								name="address" pattern="[\w \.,-_]+" type="text" /> <label
								data-error="Not correct address format" for="patient_address">Address</label>
						</div>
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">today</i><input
								id="patient_date" name="date" required type="text"
								data-type="date" />
						</div>

					</div>

				</div>

			</div>
			<div class="modal-footer">
				<a href="#!" class="btn btn-flat waves-effect waves-red"
					onclick="checkPatientUniqueAndSave(event)">Save</a>
			</div>
		</div>
	</form>

	<c:choose>
		<c:when test="${!patients.isEmpty()}">

			<main>
			<div class="parallax-container">
				<div class="parallax">
					<img src="/Project1/img/patient.jpg" />
				</div>
			</div>

			<div class="row">
				<div class="col s12 m9 l10">
					<table class="bordered highlight striped responsive-table">
						<thead>
							<tr>

								<th data-field="name">Name</th>
								<th data-field="surname">Last name</th>
								<th data-field="address">Address</th>
								<th data-field="birthday">Birthday</th>


							</tr>
						</thead>

						<tbody>
							<c:forEach var="item" items="${patients}">

								<tr>

									<td>${item.name}</td>
									<td>${item.surName}</td>
									<td>${item.address}</td>
									<td><joda:format value="${item.birthDay}"
											pattern="yyyy-MM-dd" /></td>
									<td><a
										href="${path}/DiagnosisServlet?pid=${item.id}&hid=${hid}"
										class="btn btn-flat">Diagnosis</a> <a id="edit_btn${item.id}"
										href="#modal_edit" onclick="getClicked(event)"
										data-id="${item.id }" data-name="${item.name }"
										data-surname="${item.surName }"
										data-address="${item.address }"
										data-birthday="${item.birthDay }"
										class="btn-floating modal-trigger btn blue lighten-2 tooltipped"
										data-tooltip="Edit patient" data-position="left">E</a> <a
										alt="delete" data-id="${item.id}" onclick="checkDiagnoses(event)"
										class="btn btn-small red lighten-2 btn-floating">D</a></td>

								</tr>

							</c:forEach>
						</tbody>
					</table>

				</div>



				<div class="col m3 l2 hide-on-med-and-down">

					<div class="fixed-action-btn horizontal">

						<div class="fixed-action-btn horizontal"
							style="bottom: 45px; right: 24px;">
							<a class="btn-floating btn-large red"> <i
								class="large material-icons">format_quote</i>
							</a>
							<ul>

								<li><a href="#modal_add" data-tooltip="add new patient"
									data-position="left"
									class="modal-trigger tooltipped btn-floating yellow darken-1"><i
										class="material-icons">mode_edit</i></a></li>
								<li><a href="${path}/index" class="btn-floating green"><i
										class="material-icons">publish</i></a></li>

							</ul>
						</div>
					</div>

				</div>
			</div>
			<div class="parallax-container">
				<div class="parallax">
					<img src="${path}/img/par2.jpg" alt="" />
				</div>
			</div>
			</main>
		</c:when>
		<c:when test="${ patients.isEmpty()}">
			<main>
			<p class="flow-text">There is no any patient.</p>
			</main>
		</c:when>

	</c:choose>

	<form action="${path}/EditPatient?hid=${hid}" method="post">
		<div id="modal_edit" class="modal bottom-sheet">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col s12 l12 m12">
							<h4>Edit Patient</h4>
						</div>
					</div>

					<div class="row">
						<div class="input-field col l12 m12 s12">
							<input id="p_id" name="p_id" type="hidden" length="40" required />

						</div>
					</div>
					<div class="row">
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">recent_actors</i> <input
								id="p_name" class="validate" name="p_name" type="text"
								pattern="[a-zA-Z ]+" placeholder="Name" /> <label
								data-error="Wrong pattern name" class="active" for="p_name">Name</label>
						</div>

						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">subtitles</i> <input
								id="p_surname" class="validate" pattern="[a-zA-Z ]+"
								name="p_surname" placeholder="Last Name" type="text" length="40"
								required /> <label class="active"
								data-error="Not correct last name.Must consist of alphs and space"
								for="patient_name" for="p_surname">Last Name</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">contact_phone</i> <input
								id="p_address" name="p_address" class="validate"
								pattern="[a-z0-9.,A-Z /]+" placeholder="Address" type="text"
								length="20" required /> <label class="active"
								data-error="Not correct address.Must consist of alphs and space"
								for="p_address">Address</label>
						</div>
						<div class="input-field col l6 m6 s6">
							<i class="material-icons prefix">today</i><input id="p_date"
								name="p_date" type="text" data-type="date" required />
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<input class="btn btn-flat modal-action waves-effect waves-red"
					type="submit" onclick="Materialize.toast('Patient edit',4000)"
					value="Save" />
			</div>
		</div>
	</form>
	<div id="modal_delete" class="modal">
		<div class="modal-content">
			<h4>Delete patient</h4>
			<p class="flow-text">If you delete this patient, all diagnoses
				will be removed also</p>
		</div>

		<div class="modal-footer">
			<a id="delete_reference"
				class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
		</div>
	</div>
	<div id="modal_delete" class="modal">
		<div class="modal-content">
			<h4>Delete patient</h4>
			<p class="flow-text">Do you agree with removing current
				patient?There is some diagnosis which you need to delete first.</p>
		</div>
		<div class="modal-footer">
			<a id="delete_reference"
				class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
		</div>
	</div>
	<script
		src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
	<script src="${pageContext.request.contextPath}/js/index.js"></script>
	<footer class="page-footer green">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Footer Content</h5>
				<p class="grey-text text-lighten-4">You can use rows and columns
					here to organize your footer content.</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Links</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="www.google.com">Google</a></li>
					<li><a class="grey-text text-lighten-3" href="www.vk.com">VK</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Скачать
							мультик без реестрации</a></li>
					<li><a class="grey-text text-lighten-3" href="www.twitter.com">Twitter</a></li>
					<li><a class="grey-text text-lighten-3" href="www.twitter.com">Justin
							Bieber official site</a></li>
					<li><a class="grey-text text-lighten-3" href="www.twitter.com">Boria
							Moyseev official site</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			© 2014 Copy right from site) <a
				class="grey-text text-lighten-4 right" href="#!">More Links</a>
		</div>
	</div>
	</footer>
	

</body>
</html>
