<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Hospitals</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/gen_validatorv4.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/materialize.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery_validation.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/materialize.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
	
<script src="${path}/Project1/js/hospitals.js" type="text/javascript"> </script>
<style type="text/css">
body {
	display: flex;
	min-height: 100vh;
	flex-direction: column;
}

main {
	flex: 1 0 auto;
}
</style>

</head>

<body>
	<c:set var="path" value="${pageContext.request.contextPath}" />
	<nav>
	<div class="nav nav-wrapper green darken-5">
		<div class="brand-logo right">
			<span class="white-text text-lighten-2 ">Hospitals</span>
		</div>
		<ul>
			<div>
				<a href="" data-activates="hospital_menu"
					class="dropdown-button breadcrumb"><i
					class="large material-icons">language</i> Hospitals</a>
			</div>
		</ul>

		<ul id="hospital_menu" class="dropdown-content">
			<li><a href="#modal_add" class="modal-trigger">Add hospital</a></li>
		</ul>
	</div>
	</nav>



	<form id="add_form" name="add_form" action="${path}/AddHospital"
		method="post">
		<div id="modal_add" class="modal bottom-sheet">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col s12 l12 m12">
							<h4>New Hospital</h4>
						</div>
					</div>
					<div class="row">

						<div class="input-field col l12 m12 s12">
							<i class="material-icons prefix">label</i> <input type="text"
								id="hospital_title" name="title" required maxlength="40"
								length="40" pattern="^[a-zA-Z 0-9]+$"
								class=" tooltipped validate" data-position="left"
								data-tooltip="Enter title of hospital" /> <label
								data-error="Problem with title" for="hospital_title">
								Title</label>


						</div>

					</div>
				</div>
			</div>

			<div class="modal-footer">
				<a href="#!" class="btn waves-effect waves-orange btn-flat"
					onclick="uniqueCheckAndSave(event)">Save</a>
			</div>

		</div>
	</form>

	<input value="0" type="hidden" id="delete_id" />
	<!-- Main content -->


	<div class="slider">
		<ul class="slides">
			<li><img src="${path}/img/hospital.jpg"> <!-- random image -->
				<div class="caption center-align">
					<h3 class="green-text text-lighten-5">Best hospital!</h3>
					<h5 class="red-text text-darken-5">Not only for white man.
						We love niggers</h5>
				</div></li>
			<li><img src="${path}/img/2.jpg"> <!-- random image -->
				<div class="caption right-align">
					<h3 class="green-text text-lighten-5">St. Anthony Hospital</h3>
					
				</div></li>
			<li><img src="${path}/img/3.jpg"> <!-- random image -->
				<div class="caption center-align">
					<h3 class="white-text text-lighten-5">Arrowhead Regional
						Medical Center</h3>
					
				</div></li>
		</ul>
	</div>



	<main class="container">
	<div class="row">
		<div class="col s12 m10 l10">
			<div class="section white">
				<c:forEach var="item" items="${hospitals}">
					<div class="col s4 m4 l3">
						<div id="hospitalCard"
							class="card hoverable waves-effect waves-blue">
							<div class="card-content">
								<a href='${path}/PatientsServlet?id=${item.id}'>
									<blockquote>ID: ${item.id}</blockquote>
									<h5>${item.title}</h5>
								</a>
							</div>

							<div class="card-action">
								<a id="edit_btn${item.id}" onclick="getClicked(event)"
									data-id="${item.id}" data-title="${item.title}"
									href="#modal_edit"
									class="btn btn-flat modal-trigger waves-effect waves-yellow tooltipped"
									data-tooltip="Edit hospital" data-position="right">Edit</a> <a
									data-id="${item.id}" onclick="checkPatients(event)"
									class="btn btn-flat waves-effect waves-red tooltipped"
									data-tooltip="Remove hospital" data-position="right">Remove</a>
							</div>
						</div>

					</div>

				</c:forEach>
			</div>
		</div>
		<div class="col m3 l2">
			<div class="fixed-action-btn horizontal">

				<div class="fixed-action-btn horizontal"
					style="bottom: 45px; right: 24px;">
					<a class="btn-floating btn-large red"> <i
						class="large material-icons">format_quote</i>
					</a>
					<ul>

						<li><a href="#modal_add" data-tooltip="add new hospital"
							data-position="left"
							class="modal-trigger tooltipped btn-floating yellow darken-1"><i
								class="material-icons">mode_edit</i></a></li>
						

					</ul>
				</div>
			</div>
		</div>
	</div>

	</main>




	<form name="edit_form" action="${path}/EditHospital" method="post">
		<div id="modal_edit" class="modal bottom-sheet">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col s6 l12 m12">
							<h4>Edit Hospital</h4>
						</div>
					</div>
					<input id="hospital_edit_id" name="id" type="hidden" required />

					<div class="row">

						<div class="input-field col l12 m12 s12">
							<i class="material-icons prefix">label</i> <input
								id="hospital_edit_title" class=" tooltipped validate"
								data-position="left" data-tooltip="Enter title of hospital"
								required maxlength="40" length="40" pattern="^[a-zA-Z 0-9]+$"
								name="title" type="text" /> <label for="hospital_edit_title"
								data-error="can be only alphs nums and space" class="active"></label>

						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<input class="btn btn-flat modal-action waves-effect waves-red"
					type="submit" onclick="Materialize.toast('Hospital edit',4000)"
					value="Save" />
			</div>
		</div>

	</form>

	<!-- Modal Structure -->
	<div id="modal_delete" class="modal">
		<div class="modal-content">
			<h4>Delete hospital</h4>
			<p>If you delete this hospital, all patients with diagnoses will
				be removed also</p>
		</div>

		<div class="modal-footer">
			<a id="delete_reference" href="${path}/RemoveHospital?id="
				class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
		</div>
	</div>


	<footer class="page-footer green">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Footer Content</h5>
				<p class="grey-text text-lighten-4">You can use rows and columns
					here to organize your footer content.</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Links</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="www.google.com">Google</a></li>
					<li><a class="grey-text text-lighten-3" href="www.vk.com">VK</a></li>
					<li><a class="grey-text text-lighten-3" href="www.twitter.com">Twitter</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			© 2014 Copyright Text <a class="grey-text text-lighten-4 right"
				href="#!">More Links</a>
		</div>
	</div>
	</footer>


</body>

</html>
