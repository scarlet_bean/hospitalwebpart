$('document').ready(function() {
	$('.parallax').parallax();
	$('.modal-trigger').leanModal({
		dismissible : true, // Modal can be dismissed by clicking outside of the
							// modal
		opacity : .5, // Opacity of modal background
		in_duration : 300, // Transition in duration
		out_duration : 200
	// Transition out duration});

	});

	var minDate = new Date();
	minDate.setYear(1920);

	$("#p_date").datepicker("option", "maxDate", new Date());
	$("#p_date").datepicker("option", "minDate", minDate);
	$("#p_date").datepicker("option", "dateFormat", "yy-mm-dd");

	$("#patient_date").datepicker("option", "maxDate", new Date());
	$("#patient_date").datepicker("option", "minDate", minDate);
	$("#patient_date").datepicker("option", "dateFormat", "yy-mm-dd");
});


function checkPatientUniqueAndSave(event){
	var hospital_id = $('#hos_id').val();
	var name = $('#patient_name').val();
	var surname = $('#patient_surname').val();
	var address = $('#patient_address').val();
	var date = moment($('#patient_date').val()).format("YYYY-MM-DD");
	$.get("PatientUniqueCheck",{'hid':hospital_id,
										'name':name,
										'surname':surname,
										'address':address,
										'date':date},
										function (data){
											if(data === "true"){
												$.post("AddPatient",{'hid':hospital_id,
																			'name':name,
																			'surname':surname,
																			'address':address,
																			'date':date}, function(){
																				$("#modal_add").closeModal();
																				Materialize.toast("Value added",2000);
																				setTimeout(function(){
																						window.location.reload();
																				},2000);								
																			});		
												
											}	else {
												Materialize.toast("bad",4000)
											}
										});
}

function checkDiagnoses(event) {
	var Id = $(event.target).data('id');
	
	$('#delete_reference').attr(
			"href",
			"RemovePatient?id=" + Id + "&hid="
					+ $('#hidden_hid').val());
	$.get("DiagnosesCheck", {
		id : Id
	}, function(data) {
		if (data === "not")
			$("#modal_delete").openModal();
		else {
			$.get("/RemovePatient", {
				id : Id,
				hid : $('#hidden_hid').val()
			});
			location.reload();
		}
	});
}
function getClicked(event) {
	var self = event.target;
	
	$('#p_id').val($(self).data('id'));
	$('#p_name').val($(self).data('name'));
	$('#p_surname').val($(self).data('surname'));
	$('#p_address').val($(self).data('address'));
	

	var date = $(self).data('birthday');
	$('#p_date').val(moment(date).format("YYYY-MM-DD"));
}