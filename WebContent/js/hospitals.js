/**
 * 
 */

$('document').ready(function() {
	$('.modal-trigger').leanModal({
		dismissible : true,
	});
	$('.slider').slider();
	$('.tooltipped').tooltip({
		delay : 50
	});
	$('.parallax').parallax();
});

function uniqueCheckAndSave(event) {
	var title = $('#hospital_title').val();

	$.get("HospitalUniqueCheck", {
		'title' : title
	}, function(data) {
		if (data === "true") {
			$.post("AddHospital", {
				'title' : title
			}, function() {
				$("#modal_add").closeModal();
				Materialize.toast("Value added", 2000);
				setTimeout(function() {
					window.location.reload();
				}, 2000);
			});
		} else {
			Materialize.toast("This value is not unique", 4000);
		}
	});
}
function checkPatients(event) {
	var self = event.target;
	var Id = $(self).data('id');
	$('#delete_reference').attr("href", "RemoveHospital?id=" + Id);
	$.get("HospitalCheck", {
		id : Id
	}, function(data) {
		if (data === "not") {
			$("#modal_delete").openModal();
		} else {
			$.get("RemoveHospital", {
				id : Id
			});
			location.reload();
		}
	});
}
function getClicked(event) {
	var self = event.target;
	$('#hospital_edit_id').val($(self).data('id'));
	$('#hospital_edit_title').val($(self).data('title'));
}