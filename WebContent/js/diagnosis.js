$('document').ready(function() {
	$('.modal-trigger').leanModal({
		dismissible : true, // Modal can be dismissed by clicking outside of the
		// modal
		opacity : .5, // Opacity of modal background
		in_duration : 300, // Transition in duration
		out_duration : 200
	// Transition out duration});

	});
	var minDate = new Date();
	minDate.setYear(1970);
	$("#attachment_date").datepicker("option", "maxDate", new Date());
	$("#attachment_date").datepicker("option", "minDate", minDate);
	$("#attachment_date").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#diagnosis_date").datepicker("option", "maxDate", new Date());
	$("#diagnosis_date").datepicker("option", "minDate", minDate);
	$("#diagnosis_date").datepicker("option", "dateFormat", "yy-mm-dd");
});

function getId(ev) {
	$('#d_id').val($(event.target).data('id'));
}

function setDeletingId(event) {
	$('#delete_reference').attr('href', $('#delete_reference').attr('href') + '&id=' + $(event.target).data('did'));
}